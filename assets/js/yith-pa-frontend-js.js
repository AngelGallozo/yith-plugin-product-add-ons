jQuery(function ( $ ) {

    /**Get Addon Container */
    getAddonContainer =function(element){
        return $(element).closest('.yith-wcpa-ag-addon-container');;
    },
    /**Calculate Text Price */
    calculateTextPrice = function(element){
        price_sett = $(element).data('price_settings');
        cont_addon = getAddonContainer($(element));
        msj        = $(element).val();
        addonPrice = 0;
        if ( ('free' !== price_sett) &&  (0 < msj.length ) ) {
            price_data = cont_addon.data('price');
            text_price = parseFloat(price_data);
            free_chars = parseInt( $(element).data('free_chars') );
            if ( msj.length > free_chars ) {
                switch ( price_sett ) {
                    case 'fixed':
                        addonPrice = text_price;
                        break;
                    case 'price-per-chars':
                        addonPrice = ( msj.length - free_chars ) * text_price;
                        break;
                }
            }
        }
        return addonPrice;
    },


    /**Update Price  */
    calculatePrice = function() {
        var totalPrice         = 0;
            product_price_data = yith_wcpa_ag.product_price;
            product_price      = parseFloat(product_price_data);
            symbol             = $('.yith-wcpa-ag-table-totals').data('symbol');
            addonPrice         = 0;
        $( '.yith-wcpa-ag__input' ).each( function () {
            // Checkbox and Toggle.
            if ( $( this ).is( 'input[type=checkbox]' ) && $( this ).is( ':checked' ) && ( 'free' !== $(this).data('price_settings') ) ) {
                cont_addon  = getAddonContainer($(this));
                price_data  = cont_addon.data('price');
                addonPrice  = parseFloat(price_data);
                totalPrice += addonPrice;
            }
            // Select.
            if (  $( this ).is( 'select' ) ) {
                cont_addon  = getAddonContainer($(this));
                addonSelect        = cont_addon.find( '.yith-wcpa-ag-select' );
                if ( 'free' !== $(this).data('price_settings') ) {
                    addonSelectedIndex = addonSelect.prop( 'selectedIndex' );
                    addonOptions       = addonSelect.data('options');
                    addonPrice         = parseFloat(addonOptions[addonSelectedIndex].price);
                    totalPrice += addonPrice;
                }
            }
            // Radio.
            if ( $( this ).is( 'input[type=radio]' ) && $( this ).is( ':checked' ) ) {
                price_data = $(this).data('price');
                addonPrice = parseFloat(price_data);
                totalPrice += addonPrice;
            }
            // Text.
            if ( $( this ).is( 'input[type=text]' ) || $( this ).is( 'textarea' ) ) {
                totalPrice += calculateTextPrice(this);
            }            
        });
       
        /**Update value Table Totals */
        $('.yith-wcpa-ag-additional__price').html( symbol + ' ' + totalPrice );

        // Update Total.
        $('.yith-wcpa-ag__total').html( symbol + ' ' + ( totalPrice + product_price ) );
    },

    /**Clear Add-ons of the variation */
    variationClear =function() {
        $('.yith-wcpa-ag-variation-addons .yith-wcpa-ag-addon-container').each( function() {
           $(this).remove();
       });
    },

    /**Found Variation Add-on */
    variationChange = function(event, variation){
        var post_data = {
            variation_id: variation.variation_id,
            action:       'yith_wcpa_ag_get_variation_addons',
            start_id:     $( '.yith-wcpa-ag-addon-container' ).length + 1,
        },
        productPrice = $( '.yith-wcpa-ag-additional__product-price' );

        yith_wcpa_ag.product_price = variation.display_price;
        productPrice.html( yith_wcpa_ag.product_price );
        $.ajax( {
            type: 'POST',
            dataType: 'json',
            data: post_data,
            url: yith_wcpa_ag.ajaxurl,
            success: function ( response ) {
                var variationAddons = $( '.yith-wcpa-ag-variation-addons' );
                variationAddons.html( response['addons'] );
                initializeAddon();
            },
            complete: function() {
                calculatePrice();
            },
        } );

    },

    checkboxChange = function(){
        cont_addon = getAddonContainer( $( this ) );
        check_price = cont_addon.find( '.addon-price' );
            if ( $( this ).is( ':checked' ) ) {
                check_price.slideDown();
            } else {
                check_price.slideUp();
            }
    },

    textChange =function(){
        cont_addon   = getAddonContainer( $( this ) );
        input_price  = cont_addon.find( '.addon-price' );
        input_plus   = cont_addon.find( '.addon-price-plus' );
        input_symbol = cont_addon.find( '.addon-price-symbol' );
        text_price   = calculateTextPrice($(this));
        $(input_price).html(parseFloat(text_price));
        if ( 0 < text_price ) {
            input_price.slideDown();
            input_plus.slideDown();
            input_symbol.slideDown();
        } else {
            input_price.slideUp()
            input_plus.slideUp();
            input_symbol.slideUp();
        }
    },

    initializeAddon = function(){
         // Initialize selects.
         $( '.yith-wcpa-ag-select' ).each( function() {
            $(this).attr('selected','selected');
        });
        // Initialize Input Radios.
        $( '.yith-wcpa-ag-addon__radio' ).each( function() {
            $($(this).find('.yith-wcpa-ag-radio').first()).attr('checked','checked');
        });
        // Initialize all inputs.
        $('.yith-wcpa-ag__input').trigger('change');
        $('.yith-wcpa-ag__input').trigger('keyup');
    },

    /**Change Onoff*/
    $(document).
        on('change', '.yith-wcpa-ag-onoff', checkboxChange).
        on('change', '.yith-wcpa-ag-onoff', calculatePrice);

    /**Change Checkbox*/
    $(document).
        on('change', '.yith-wcpa-ag-checkbox', checkboxChange).
        on('change', '.yith-wcpa-ag-checkbox', calculatePrice);

    /**Change Select*/
    $(document).on('change', '.yith-wcpa-ag-select', calculatePrice);
    
    /**Change Radio*/
    $(document).on('change', '.yith-wcpa-ag-radio:checked', calculatePrice);

    /**Change Text && TextArea*/
    $(document).
        on('keyup', '.yith-wcpa-ag-text', textChange).
        on('keyup', '.yith-wcpa-ag-text', calculatePrice);

    /**Found Variation Add-ons */
    $(document).on('found_variation', variationChange);

    /*Clear Add-ons Variations */
    $(document).on('reset_data', variationClear);

    jQuery(document).ready(function ( $ ) {
        initializeAddon();
    });
});

