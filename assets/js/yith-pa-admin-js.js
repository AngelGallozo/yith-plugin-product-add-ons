jQuery(function ( $ ) {
    var index=0; //Global Index


    /**Get main Add-ons Container */
    getMainAddonCont= function( element){
        return element.closest('.yith-wcpa-ag-main-container');
    },
    /**Get Add-on container */
    getAddonContParent= function( element){
        return element.closest('.yith-wcpa-ag-addon-container');
    },
    /**Get add-on Body */
    getAddonBody= function(element){
        return $(element).find('.yith-wcpa-ag-addon-body');
    },
    /**Function Slide Panel */
    slidePanel= function(){
        addon=getAddonContParent(this);
        $(addon).find('.yith-wcpa-ag-remove-addon').slideToggle("fast");
        body=getAddonBody(addon);
        body.slideToggle("slow");
    },

    scrollTo =function( element){
        //$('html,body').animate({scrollTop: element.offset()}, 2000 );
    },

    /**Function Add new Add-on */
    addNewAddon= function(){
        var mainContainer=getMainAddonCont($(this));
            siteAddons=mainContainer.find('.yith-wcpa-ag-addons');
            addontemplate=mainContainer.find('.yith-wcpa-ag-template');
            newAddon=$(addontemplate.html().replaceAll('{{INDEX}}',index));
            newIndexAddon=newAddon.find('.yith-wcpa-ag-addon-index');
        newIndexAddon.attr('value',index);
        siteAddons.append(newAddon);
        initializeAddon(newAddon);
        newAddon.find('.yith-wcpa-ag-addon').show();
        //scrollTo($(newAddon));
        index++;
    },

    /**Function Remove Add-on */
    removeAddon= function(){
        var addonDel= getAddonContParent($(this));
        $(addonDel).remove();
         //Update Addons index
         index = 0;
         $( '.yith-wcpa-ag-addon-container' ).each(
             function() {
                 var indexInput = $( this ).find(
                     '.yith-wcpa-ag-addon-index'
                 );
                 indexInput.attr(
                     'value',
                     index
                 );
                 index++;
             });
    },

    /**Function Initialize new Add-on */
    initializeAddon = function( element ){
        element.find('.yith-wcpa-ag-addon_field-type').trigger('change');
        $(element).find('.yith-wcpa-ag-remove-addon').slideToggle("fast");
        body=getAddonBody(element);
        $(body).slideToggle("fast");

    }

    
    /**Add new Select Option */
    addNewSelectOption = function(){
        cont_select_option=$(this).closest('div');
        cont_option=cont_select_option.find('.yith-wcpa-ag-select-options').first();
        new_cont_option=cont_option.clone();
        new_cont_option.find('.yith-wcpa-ag-delete-option').show();
        new_cont_option.find('.yith-wcpa-ag-select-option__text-input').val('');
        new_cont_option.find('.yith-wcpa-ag-select-option__price-input').val(0);
        cont_select_option.append(new_cont_option);
        cont_select_option.append(this);
    },

    /**Delete Select Option */
    deleteSelectOption= function(){
        cont_option=$(this).closest('div');
        cont_option.remove();
    },

    /**Modify Name Addon */
    modifyNameAddon = function(){
        cont_addon=$(getAddonContParent(this));
        label=$(cont_addon.find('.yith-wcpa-ag-title-addon'));
        label.text($(this).val());
    },

    /**Function Select Change */
    changeSelect = function(){
        addon=$(getAddonContParent(this));
        cont_options=addon.find('.yith-wcpa-ag-container-select-options');
        cont_checkbox=addon.find('.yith-wcpa-ag-container-checkbox-options');
        option_ppc=addon.find('.yith-wcpa-ag-radio-ppc').closest('span');
        cont_inpt_free_c=addon.find('.yith-wcpa-ag-field-container__free-chars');
        cont_inpt_price=addon.find('.yith-wcpa-ag-field-container__price');

        if ('text' === $(this).val() || 'textarea' === $(this).val()) {
            cont_options.hide();
            cont_checkbox.hide();
            cont_inpt_free_c.show();
            cont_inpt_price.show();
            option_ppc.show();
        }

        if ('radio' === $(this).val() || 'select' === $(this).val()) {
            cont_options.show();
            cont_options.find('.yith-wcpa-ag-delete-option').first().hide();
            cont_checkbox.hide();
            option_ppc.hide();
            cont_inpt_free_c.hide();
            cont_inpt_price.hide();
        }

        if ('checkbox' === $(this).val() || 'onoff' === $(this).val()) {
            cont_checkbox.show();
            cont_options.hide();
            option_ppc.hide();
            cont_inpt_price.show();
            cont_inpt_free_c.hide();
        }
        
        // Change options.
        addon.find('.yith-wcpa-ag-radio__input').trigger('change');
    },

    /** Function Radio Prices Change */
    changePrice = function(){
        if ( $(this).is(':checked') ) {
            addon=$(getAddonContParent(this));
            field_type=addon.find('.yith-wcpa-ag-addon_field-type').val();
            cont_inpt_free_c=addon.find('.yith-wcpa-ag-field-container__free-chars');
            cont_inpt_price=addon.find('.yith-wcpa-ag-field-container__price');
            cont_select_opt_price=addon.find('.yith-wcpa-ag-select-option__price');
            switch ($(this).val()){
                case 'free': 
                    cont_inpt_price.hide();
                    cont_inpt_free_c.hide();
                    if('select' === field_type || 'radio' === field_type){
                        cont_select_opt_price.hide();
                    }
                    break;
                case 'fixed':
                    cont_inpt_price.show();
                    if('text' === field_type || 'textarea' === field_type){
                        cont_inpt_free_c.show();
                    }else{
                        cont_inpt_free_c.hide();
                    }
                    
                    if('select' === field_type || 'radio' === field_type ){
                        cont_select_opt_price.show();
                        cont_inpt_price.hide();
                    }
                    break;
                case 'price-per-chars':
                    if('text' === field_type || 'textarea' === field_type){
                        cont_inpt_price.show();
                        cont_inpt_free_c.show();
                    }else{
                         // Change Price to Free
                         addon.find('.yith-wcpa-ag-radio-free').prop("checked", true);
                         addon.find('.yith-wcpa-ag-radio__input').trigger('change');
                    }
                    break;
            }
        }
    },

    /**Initialize Fields in Add-on of the Variation*/
    initializeVariation = function(){
        getAddonBody($('.yith-wcpa-ag-template')).slideUp("fast");
         $('.yith-wcpa-ag-template').find('.yith-wcpa-ag-remove-addon').slideUp("fast");

        /**Makes de Add-on Sortable */
        $('.yith-wcpa-ag-addons').sortable({
            handle:'.yith-wcpa-ag-addon-header', item:'.yith-wcpa-ag-addon-container',
            update: function( event, ui ) {
                index = 0;
                $( '.yith-wcpa-ag-addon-container' ).each(
                    function() {
                        var indexInput = $( this ).find(
                            '.yith-wcpa-ag-addon-index'
                        );
                        indexInput.attr(
                            'value',
                            index
                        );
                        index++;
                    }
                );
            }
        });
        /**Initialize Add-ons and global index*/
        $( '.yith-wcpa-ag-addons .yith-wcpa-ag-addon-container' ).each( function() {
            $( this ).html( $( this ).html().replaceAll( '{{INDEX}}', index ) );
            index++;
            initializeAddon( $( this ) );

        });
    },

     /**Add Add-on Click*/
     $(document).on('click', '.yith-wcpa-ag-add-new-addon', addNewAddon);

     /**Slide Panel Click*/
     $(document).on('click', '.yith-wcpa-ag-btn-slide', slidePanel);

     /**Slide Panel Click*/
     $(document).on('click', '.yith-wcpa-ag-remove-addon', removeAddon);
      
     /**Select Option change*/
     $(document).on('change', '.yith-wcpa-ag-addon_field-type', changeSelect);

     /**Select Prices change*/
     $(document).on('change', '.yith-wcpa-ag-radio__input', changePrice);

    /**Add new Select Option*/
    $(document).on('click', '.yith-wcpa-ag-add-new-option-select', addNewSelectOption);

    /**Delete Select Option */
    $(document).on('click', '.yith-wcpa-ag-delete-option', deleteSelectOption);

    /**Modify Name Addon */
    $(document).on('keyup','.yith-wcpa-ag-name', modifyNameAddon);
    
    /**Initialize Addons of the Variations */
    $(document).on('woocommerce_variations_loaded', initializeVariation);

     /**Initialize Container */
    jQuery(document).ready(function ( $ ) {
         getAddonBody($('.yith-wcpa-ag-template')).slideUp("fast");
         $('.yith-wcpa-ag-template').find('.yith-wcpa-ag-remove-addon').slideUp("fast");

        /**Makes de Add-on Sortable */
        $('.yith-wcpa-ag-addons').sortable({
            handle:'.yith-wcpa-ag-addon-header', item:'.yith-wcpa-ag-addon-container',
            update: function( event, ui ) {
                index = 0;
                $( '.yith-wcpa-ag-addon-container' ).each(
                    function() {
                        var indexInput = $( this ).find(
                            '.yith-wcpa-ag-addon-index'
                        );
                        indexInput.attr(
                            'value',
                            index
                        );
                        index++;
                    }
                );
            }
        });
        /**Initialize Add-ons and global index*/
        $( '.yith-wcpa-ag-addons .yith-wcpa-ag-addon-container' ).each( function() {
            $( this ).html( $( this ).html().replaceAll( '{{INDEX}}', index ) );
            index++;
            initializeAddon( $( this ) );

        });
    });
});

