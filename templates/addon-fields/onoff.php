<?php //phpcs:ignore
/**Addon type toggle */
?>

<div class="yith-wcpa-ag-addon-container" data-price = "<?php echo ( ( 'free' !== $addon['price_settings'] ) && isset( $addon['price'] ) ) ? esc_html( $addon['price'] ) : 0; ?>">
	<div class="yith-wcpa-ag-addon__onoff">
		<label class="lb_addon" for="yith-wcpa-ag-onoff-<?php echo ( esc_html( $addon['index'] ) ); ?>"> <?php echo ( ( isset( $addon['name'] ) ) ? esc_html( $addon['name'] ) : 'Untitled' ); ?> </label>
		<p> 
		<label class="yith-wcpa-ag__toggle-field-input__toggle">
			<input type="checkbox" class="yith-wcpa-ag__toggle-field-input__input  yith-proteo-standard-checkbox yith-wcpa-ag__input yith-wcpa-ag-onoff" name="yith_wcpa_ag_addons[<?php echo ( esc_html( $addon['index'] ) ); ?>]" id="yith-wcpa-ag-onoff-<?php echo ( esc_html( $addon['index'] ) ); ?>"
				<?php checked( isset( $addon['default_enabled'] ) && 'yes' === $addon['default_enabled'] ); ?> <?php checked( isset( $addon['default_enabled'] ) && ( 'yes' === $addon['default_enabled'] ) ); ?>
				data-price_settings="<?php echo ( isset( $addon['price_settings'] ) ? esc_html( $addon['price_settings'] ) : '' ); ?>" value="yes">
			<span class="yith-wcpa-ag__toggle-field-input__toggle-btn round"></span>
		</label>
		<?php echo ( ( isset( $addon['description'] ) ) ? esc_html( $addon['description'] ) : '' ); ?> </p>
		<?php if ( ( 'free' !== $addon['price_settings'] ) && isset( $addon['price'] ) ) { ?>
			<span class="addon-price"> <?php echo ( '+ ' . wc_price( $addon['price']  ) ); ?></span>
		<?php } ?>
	</div>
</div>
