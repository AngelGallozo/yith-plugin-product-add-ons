<?php //phpcs:ignore
/**Addon type Checkbox */
?>

<div class="yith-wcpa-ag-addon-container" data-price = "<?php echo ( ( 'free' !== $addon['price_settings'] ) && isset( $addon['price'] ) ) ? esc_html( $addon['price'] ) : 0; ?>">
	<div class="yith-wcpa-ag-addon__checkbox">
		<label class="lb_addon" for="yith-wcpa-ag-checkbox-<?php echo ( esc_html( $addon['index'] ) ); ?>"> <?php echo ( ( isset( $addon['name'] ) ) ? esc_html( $addon['name'] ) : 'Untitled' ); ?> </label>
		<p> <input type="checkbox" class="yith-wcpa-ag__input yith-wcpa-ag-checkbox" name="yith_wcpa_ag_addons[<?php echo ( esc_html( $addon['index'] ) ); ?>]" id="yith-wcpa-ag-checkbox-<?php echo ( esc_html( $addon['index'] ) ); ?>" <?php checked( isset( $addon['default_enabled'] ) && ( 'yes' === $addon['default_enabled'] ) ); ?>
			data-price_settings="<?php echo ( isset( $addon['price_settings'] ) ? esc_html( $addon['price_settings'] ) : '' ); ?>" value="yes">
			<?php echo ( ( isset( $addon['description'] ) ) ? esc_html( $addon['description'] ) : '' ); ?> </p>
			<?php if ( ( 'free' !== $addon['price_settings'] ) && isset( $addon['price'] ) ) { ?>
				<span class="addon-price"> <?php echo ( '+ ' . wc_price( $addon['price'] ) ) ); ?></span>
			<?php } ?>
	</div>
</div>
