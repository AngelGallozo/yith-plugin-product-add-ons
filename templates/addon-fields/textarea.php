<?php //phpcs:ignore
/**Addon type TextArea */
?>

<div class="yith-wcpa-ag-addon-container" data-price = "<?php echo ( ( 'free' !== $addon['price_settings'] ) && isset( $addon['price'] ) ) ? esc_html( $addon['price'] ) : 0; ?>">
	<div class="yith-wcpa-ag-addon__text-area">
		<label class="lb_addon" for="yith_wcpa_ag_text-area-<?php echo ( esc_html( $addon['index'] ) ); ?>"> <?php echo ( ( isset( $addon['name'] ) ) ? esc_html( $addon['name'] ) : 'Untitled' ); ?> </label>
		<p> <?php echo ( ( isset( $addon['description'] ) ) ? esc_html( $addon['description'] ) : '' ); ?> </p>
		<?php if ( ( 'free' !== $addon['price_settings'] ) && isset( $addon['price'] ) ) { ?>
			<p>
				<span class="addon-price-plus">+</span> <span class="addon-price-symbol"><?php echo esc_html( get_woocommerce_currency_symbol() ); ?></span> <span class="addon-price"> <?php echo ( isset( $addon['price'] ) ? esc_html( $addon['price'] ) : 0 ); ?></span>
			</p>
		<?php } ?>
		<textarea name="yith_wcpa_ag_addons[<?php echo ( esc_html( $addon['index'] ) ); ?>]" id="yith_wcpa_ag_text-area-<?php echo ( esc_html( $addon['index'] ) ); ?>" class="yith-wcpa-ag-text yith-wcpa-ag__input" data-free_chars="<?php echo ( ( 'free' !== $addon['price_settings'] ) && isset( $addon['free_chars'] ) ) ? esc_html( $addon['free_chars'] ) : 0; ?>"
			data-price_settings="<?php echo ( isset( $addon['price_settings'] ) ? esc_html( $addon['price_settings'] ) : '' ); ?>"></textarea>
	</div>
</div>
