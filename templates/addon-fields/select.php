<?php //phpcs:ignore
/**Addon type Select */
$data = json_encode( $addon['options'] );
?>

<div class="yith-wcpa-ag-addon-container">
	<div class="yith-wcpa-ag-addon__select" >
		<label class="lb_addon" for="yith-wcpa-ag-select-<?php echo ( esc_html( $addon['index'] ) ); ?>"> <?php echo ( ( isset( $addon['name'] ) ) ? esc_html( $addon['name'] ) : 'Untitled' ); ?> </label>
		<p><?php echo ( ( isset( $addon['description'] ) ) ? esc_html( $addon['description'] ) : '' ); ?></p>
		<select id="yith-wcpa-ag-select-<?php echo ( esc_html( $addon['index'] ) ); ?>" class="yith-wcpa-ag__input yith-wcpa-ag-select" id="yith-wcpa-ag-select-<?php echo ( esc_html( $addon['index'] ) ); ?>" data-options="<?php echo esc_html( $data ); ?>"
			data-price_settings="<?php echo ( isset( $addon['price_settings'] ) ? esc_html( $addon['price_settings'] ) : '' ); ?>" name="yith_wcpa_ag_addons[<?php echo ( esc_html( $addon['index'] ) ); ?>]">
			<?php foreach ( $addon['options'] as $option ) : ?>       
				<option value="<?php echo ( ( isset( $option['name'] ) ) ? esc_html( $option['name'] ) : '' ); ?>" data-price = "<?php echo ( ( isset( $addon['price_settings'] ) ) && ( 'free' !== $addon['price_settings'] ) && ( isset( $option['price'] ) ) ) ? esc_html( $option['price'] ) : 0; ?>"> 
					<?php echo ( esc_html( $option['name'] ) . ' ' . ( isset( $addon['price_settings'] ) && ( 'free' !== $addon['price_settings'] ) && isset( $option['price'] ) && ( 0 < $option['price'] ) ? '(+ ' . wc_price( $option['price'] ) . ')' : '' ) ); ?>
				</option> 
		<?php endforeach; ?>
		</select>
	</div>
</div>
