<?php //phpcs:ignore
/**Addon type Radio */
$data = json_encode( $addon['options'] );
?>

<div class="yith-wcpa-ag-addon-container">
	<div class="yith-wcpa-ag-addon__radio">
		<label class="lb_addon"> <?php echo ( ( isset( $addon['name'] ) ) ? esc_html( $addon['name'] ) : 'Untitled' ); ?> </label>
		<p><?php echo ( ( isset( $addon['description'] ) ) ? esc_html( $addon['description'] ) : '' ); ?></p>
		<?php foreach ( $addon['options'] as $option ) : ?>    
			<label class="yith-wcpa-ag__toggle-field-input__radio">
				<input type="radio" value="<?php echo ( ( isset( $option['name'] ) ) ? esc_html( $option['name'] ) : '' ); ?>" 
					id="yith-wcpa-ag-radio-" class="yith-wcpa-ag__input yith-wcpa-ag-radio"
					name="yith_wcpa_ag_addons[<?php echo ( esc_html( $addon['index'] ) ); ?>]" data-price = "<?php echo ( ( 'free' !== $addon['price_settings'] ) && isset( $option['price'] ) ) ? esc_html( $option['price'] ) : 0; ?>"
					data-price_settings="<?php echo ( isset( $addon['price_settings'] ) ? esc_html( $addon['price_settings'] ) : '' ); ?>">
					<?php echo ( esc_html( $option['name'] ) . ' ' . ( isset( $addon['price_settings'] ) && ( 'free' !== $addon['price_settings'] ) && isset( $option['price'] ) && ( 0 < $option['price'] ) ? '(+ ' . wc_price( $option['price'] ) . ')' : '' ) ); ?>
			</label>
		<?php endforeach; ?>
	</div>
</div>
