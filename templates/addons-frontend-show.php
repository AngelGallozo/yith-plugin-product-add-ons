<?php //phpcs:ignore 
/**Add-on in Frontend */ 
$show_table = false;
?>
<?php if ( count( $addons ) > 0 ) { ?>
	<div class="yith-wcpa-ag-addons">
	<div class="yith-wcpa-ag-variation-addons"></div>
	<?php
	foreach ( $addons as $addon ) :
		if ( isset( $addon['enabled'] ) && 'yes' === $addon['enabled'] ) {
			$show_table = true;
			$addon['product_id'] = $product->get_id();
			wc_get_template( $addon['field_type'] . '.php', compact( 'addon' ), '', YITH_PA_DIR_TEMPLATES_PATH . '/addon-fields/' );
		}
		endforeach;
	?>
	<?php if ( $show_table ) { ?>
		<div class="yith-wcpa-ag-table-totals" data-symbol="<?php echo esc_attr( get_woocommerce_currency_symbol() ); ?>" >
			<div class="yith-wcpa-ag-table-title">
				<label> <?php esc_html_e( 'Price Totals', 'yith-plugin-product-addons' ); ?></label>
				<div class="yith-wcpa-ag-product-price">
					<label> <?php esc_html_e( 'Product price', 'yith-plugin-product-addons' ); ?></label>
					<span class="yith-wcpa-ag-additional__product-price"><?php echo ( esc_html( get_woocommerce_currency_symbol() ) . esc_html( $product->get_price() ) ); ?></span>
				</div>
				<div class="yith-wcpa-ag-additional-price">
					<label> <?php esc_html_e( 'Additional options price', 'yith-plugin-product-addons' ); ?></label>
					<span class="yith-wcpa-ag-additional__price"><?php echo ( esc_html( wc_price( 0 ) ) ); ?></span>
				</div>
				<div class="yith-wcpa-ag-total">
					<label> <?php esc_html_e( 'Total', 'yith-plugin-product-addons' ); ?></label>
					<span class="yith-wcpa-ag__total"><?php echo ( esc_html( wc_price( 0 ) ) ); ?></span>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
<?php } ?>
