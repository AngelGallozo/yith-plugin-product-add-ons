<?php

/**
 * Plugin Name: YITH Plugin Product Addons
 * Description: Products Addons for YITH Plugins
 * Version: 1.0.0
 * Author: Angel Gallozo
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-product-addons
 */

! defined( 'ABSPATH' ) && exit;  // check if defined ABSPATH.

/* where defined PATH for Style, Assets, Templates, Views */
if ( ! defined( 'YITH_PA_VERSION' ) ) {
	define( 'YITH_PA_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PA_DIR_URL' ) ) {
	define( 'YITH_PA_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PA_DIR_PATH' ) ) {
	define( 'YITH_PA_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PA_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PA_DIR_ASSETS_URL', YITH_PA_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PA_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PA_DIR_ASSETS_CSS_URL', YITH_PA_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PA_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PA_DIR_ASSETS_JS_URL', YITH_PA_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PA_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PA_DIR_INCLUDES_PATH', YITH_PA_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PA_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PA_DIR_TEMPLATES_PATH', YITH_PA_DIR_PATH . '/templates' );
}


if ( ! defined( 'YITH_PA_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PA_DIR_VIEWS_PATH', YITH_PA_DIR_PATH . 'views' );
}

if ( ! function_exists( 'yith_pa_init_classes' ) ) {
	/**
	 * Include the scripts
	 *
	 * @return void
	 */
	function yith_pa_init_classes() {

		load_plugin_textdomain( 'yith-plugin-product-addons', false, basename( dirname( __FILE__ ) ) . '/languages/' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PA_DIR_INCLUDES_PATH . '/class-yith-pa-product-addons.php';

		if ( class_exists( 'YITH_PA_Plugin_Product_Addons' ) ) {
			/*
			*	Call the main function
			*/
			yith_pa_plugin_product_addons();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pa_init_classes', 11 );
