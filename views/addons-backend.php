<?php //phpcs:ignore
/** Backend Addons container */
$default_addons_values = array(
	'enabled'         => 'yes',
	'index'           => 0,
	'name'            => __( 'Untitled', 'yith-plugin-product-addons' ),
	'description'     => '',
	'field_type'      => 'text',
	'price_settings'  => 'free',
	'price'           => '0',
	'free_chars'      => 0,
	'options'         => array(
		array(
			'name'  => '',
			'price' => 0,
		),
	),
	'default_enabled' => 'yes',
	'loop'            => 0,

);
$def_args = array( 'addon' => $default_addons_values );
if ( isset( $loop ) ) {
		$def_args['loop'] = $loop + 1;
}
?>

<div class="yith-wcpa-ag-main-container">
	<span class="yith-wcpa-ag-add-new-addon"> <?php esc_html_e( 'Add new add-on', 'yith-plugin-product-addons' ); ?></span>
	<div class="yith-wcpa-ag-template" hidden>
		<?php yith_wcpa_get_view( '/addon-panel-model.php', $def_args ); ?>
	</div>
	<div class="yith-wcpa-ag-addons">
		<?php
		if ( isset( $addons ) ) {
			foreach ( $addons as $addon ) {
				$args = array( 'addon' => $addon );
				if ( isset( $loop ) ) {
					$args['loop'] = $loop + 1;
				}
				yith_wcpa_get_view( '/addon-panel-model.php', $args );
			}
		}
		?>
	</div>
</div>
