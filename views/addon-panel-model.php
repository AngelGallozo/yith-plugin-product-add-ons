<?php //phpcs:ignore
/**Addon Backend Template */
$loop        = isset( $loop ) ? $loop : 0;
$field_types = array( 'text', 'textarea', 'select', 'radio', 'checkbox', 'onoff' );
?>

<div class="yith-wcpa-ag-addon-container" id="yith-wcpa-ag-addon-container">
	<div class="yith-wcpa-ag-addon-header">
	<span class="dashicons dashicons-arrow-down-alt2 yith-wcpa-ag-btn-slide"></span>
	<label class="yith-wcpa-ag-title-addon"><?php echo esc_html( isset( $addon['name'] ) ? $addon['name'] : 'Untitled' ); ?></label>
		<label class="yith-wcpa-ag__toggle-field-input__toggle">
			<input type="checkbox" id="yith-wcpa-ag-addon-enabled-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag__toggle-field-input__input yith-addon-enable" name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][enabled]"
				value="yes" <?php checked( isset( $addon['enabled'] ) && 'yes' === $addon['enabled'] ); ?>>
				<span class="yith-wcpa-ag__toggle-field-input__toggle-btn round"></span>
		</label>
	</label>
	</div>
	<div class="yith-wcpa-ag-addon">
		<div class="yith-wcpa-ag-addon-body">
			<!-- Index field (hidden)-->
			<input type="hidden" class="yith-wcpa-ag-addon-index" name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][index]"
				value="<?php echo intval( isset( $addon['index'] ) ? $addon['index'] : -1 ); ?>">
			<!-- Name field (text)-->
			<div class="yith-wcpa-ag-field_container">
				<label for="yith-wcpa-ag-name-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__label">
					<?php esc_html_e( 'Name', 'yith-plugin-product-addons' ); ?></label>
				<div class="yith-wcpa-ag-input-container">
					<input type="text" id="yith-wcpa-ag-name-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__input yith-wcpa-ag-name"
						name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][name]"
						value="<?php echo esc_html( isset( $addon['name'] ) ? $addon['name'] : '' ); ?>">
					<div class="yith-wcpa-ag-field-description"><?php esc_html_e( 'The name of the Add-on', 'yith-plugin-product-addons' ); ?></div>
				</div>
			</div>
			<!-- Description field (textarea)-->
			<div class="yith-wcpa-ag-field_container">
				<label for="yith-wcpa-ag-desc-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__label">
					<?php esc_html_e( 'Description', 'yith-plugin-product-addons' ); ?></label>
				<div class="yith-wcpa-ag-input-container">
					<textarea id="yith-wcpa-ag-desc-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__input yith-wcpa-ag-description"
						name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][description]"><?php echo esc_html( isset( $addon['description'] ) ? $addon['description'] : '' ); ?> </textarea>
					<div class="yith-wcpa-ag-field-description"><?php esc_html_e( 'Set here the description that will be shown above the add-on field', 'yith-plugin-product-addons' ); ?></div>
				</div>
			</div>
			<!-- Field Type field (select)-->
			<div class="yith-wcpa-ag-field_container">
				<label for="yith-wcpa-ag-desc-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__label">
					<?php esc_html_e( 'Field Type', 'yith-plugin-product-addons' ); ?></label>
				<div class="yith-wcpa-ag-input-container">
					<select id="yith-wcpa-ag-field-type-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__input yith-wcpa-ag-addon_field-type" 
						name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][field_type]">
						<?php foreach ( $field_types as $field_type ) : ?>    
							<option value="<?php echo esc_html( $field_type ); ?>" <?php echo selected( isset( $addon['field_type'] ) && $addon['field_type'] === $field_type ); ?>> 
								<?php echo esc_html( $field_type ); ?>
							</option>
						<?php endforeach; ?>    
					</select>
					<div class="yith-wcpa-ag-field-description"><?php esc_html_e( 'Set here the description that will be shown above the add-on field', 'yith-plugin-product-addons' ); ?></div>
				</div>
			</div>
			<!-- Price Settings field (radio)-->
			<div class="yith-wcpa-ag-field_container">
				<label for="yith-wcpa-ag-radio-free-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__label">
					<?php esc_html_e( 'Price Settings', 'yith-plugin-product-addons' ); ?></label>
				<div class="yith-wcpa-ag-input-container">
					<div class="yith-wcpa-ag-radio-group">
						<!-- Price Free-->
						<span class="yith-wcpa-ag-single-radio-container yith-wcpa-ag-field__input">
							<label>
								<input type="radio" value="free" id="yith-wcpa-ag-radio-free-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-radio__input yith-wcpa-ag-radio-free"
									name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][price_settings]" <?php checked( isset( $addon['price_settings'] ) && 'free' === $addon['price_settings'] ); ?>>
									<?php esc_attr_e( 'free', 'yith-plugin-product-addons' ); ?>
							</label>
						</span>
						<!-- Price Fixed-->
						<span class="yith-wcpa-ag-single-radio-container yith-wcpa-ag-field__input">
							<label>
								<input type="radio" value="fixed" id="yith-wcpa-ag-radio-fixed-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-radio__input yith-wcpa-ag-radio-fixed-price"
									name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][price_settings]" <?php checked( isset( $addon['price_settings'] ) && 'fixed' === $addon['price_settings'] ); ?>>
									<?php esc_attr_e( 'fixed', 'yith-plugin-product-addons' ); ?>
							</label>
						</span>
						<!-- Price per Character-->
						<span class="yith-wcpa-ag-single-radio-container yith-wcpa-ag-field__input">
							<label>
								<input type="radio" value="price-per-chars" id="yith-wcpa-ag-radio-ppc-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-radio__input yith-wcpa-ag-radio-ppc"
									name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][price_settings]" <?php checked( isset( $addon['price_settings'] ) && 'price-per-chars' === $addon['price_settings'] ); ?>>
									<?php esc_attr_e( 'Price per Chars', 'yith-plugin-product-addons' ); ?>
							</label>
						</span>
					</div>
					<div class="yith-wcpa-ag-field-description"><?php esc_html_e( 'Choose how the add-on will influence the product price', 'yith-plugin-product-addons' ); ?></div>
				</div>
			</div>
			<!-- Price Field (text)-->
			<div class="yith-wcpa-ag-field_container yith-wcpa-ag-field-container__price">
				<label for="yith-wcpa-ag-price-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__label">
					<?php esc_html_e( 'Price', 'yith-plugin-product-addons' ); ?></label>
				<div class="yith-wcpa-ag-input-container">
					<input type="text" id="yith-wcpa-ag-price-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__input yith-wcpa-ag-price"
						name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][price]"
						value="<?php echo esc_html( isset( $addon['price'] ) ? $addon['price'] : '' ); ?>">
					<div class="yith-wcpa-ag-field-description"><?php esc_html_e( 'Set price of text field selected', 'yith-plugin-product-addons' ); ?></div>
				</div>
			</div>
			<!-- Free Characters Field (number)-->
			<div class="yith-wcpa-ag-field_container yith-wcpa-ag-field-container__free-chars">
				<label for="yith-wcpa-ag-free-chars-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__label">
					<?php esc_html_e( 'Free Characters', 'yith-plugin-product-addons' ); ?></label>
				<div class="yith-wcpa-ag-input-container">
					<input type="number" min="0" id="yith-wcpa-ag-free-chars-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag-field__input"
						name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][free_chars]"
						value="<?php echo esc_html( isset( $addon['free_chars'] ) ? $addon['free_chars'] : 0 ); ?>">
					<div class="yith-wcpa-ag-field-description"><?php esc_html_e( 'Choose the numbers od free characters', 'yith-plugin-product-addons' ); ?></div>
				</div>
			</div>
			<!-- Select Options -->
			<div class="yith-wcpa-ag-container-select-options">
			<label for="yith-wcpa-ag-options__label">Options</label>
				<?php foreach ( $addon['options'] as $op ) : ?>
					<div class="yith-wcpa-ag-select-options">
						<span class="yith-wcpa-ag-select-option__text">
							<label for="yith-wcpa-ag-select-option-text" class="yith-wcpa-field-title">Name</label>
							<input type="text" class="yith-wcpa-ag-select-option__text-input" name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][options][text][]"
								value="<?php echo esc_html( isset( $op['name'] ) ? $op['name'] : '' ); ?>">
						</span>
						<span class="yith-wcpa-ag-select-option__price">
							<label for="yith-wcpa-ag-select-option-price" class="yith-wcpa-field-title">Price</label>
							<input type="text" class="yith-wcpa-ag-select-option__price-input" name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][options][price][]"
								value="<?php echo esc_html( isset( $op['price'] ) ? $op['price'] : '' ); ?>">
						</span>
						<span class="dashicons dashicons-trash yith-wcpa-ag-delete-option"></span>
					</div>
				<?php endforeach; ?>
				<p class="yith-wcpa-ag-add-new-option-select">+Add new Option</p>
			</div>
			<!-- CheckBox Options -->
			<div class="yith-wcpa-ag-container-checkbox-options">
				<label for="yith-wcpa-ag-addon-checkbox-options-<?php echo esc_attr( $loop ); ?>-{{INDEX}}"> Default Enabled</label>
				<label class="yith-wcpa-ag__toggle-field-input__toggle">
					<input type="checkbox" id="yith-wcpa-ag-addon-checkbox-option-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-wcpa-ag__toggle-field-input__input yith-addon-enable" name="yith_wcpa_ag_addons[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][default_enabled]"
						value="yes" <?php checked( isset( $addon['default_enabled'] ) && 'yes' === $addon['default_enabled'] ); ?>>
					<span class="yith-wcpa-ag__toggle-field-input__toggle-btn round"></span>
				</label>
			</div>
		</div>
	</div>
	<div class="yith-wcpa-ag-container-remove-addon">
		<span  class="yith-wcpa-ag-remove-addon"> <?php esc_html_e( 'Remove add-on', 'yith-plugin-product-addons' ); ?></span>
	</div>
</div>
