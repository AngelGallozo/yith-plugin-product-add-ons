<?php //phpcs:ignore
/**
 * This file belongs to the YITH PA Plugin Product Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PA_PO' ) ) {
	/**
	 * YITH_PA_Product_Options
	 */
	class YITH_PA_PO {
		/**
		 * Main Instance
		 * 
		 * @var YITH_PA_PO
		 * @since 1.0.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Main plugin Instance
		 * @return YITH_PA_Admin Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PA_Product_Options constructor.
		 */
		private function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_wcpa_ag_add_addons_options' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'yith_wcpa_ag_print_addons_options' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'yith_wcpa_ag_save_addons_options' ) );

			// Variations.
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'yith_wcpa_ag_add_addon_options_variations' ), 10, 3 );
			add_action( 'woocommerce_admin_process_variation_object', array( $this, 'yith_wcpa_ag_save_addon_options_variations' ), 10, 2 );
		}
		/**
		 * Add Add-ons Section to the Options
		 * @param  mixed $tabs
		 * @return void
		 */
		public function yith_wcpa_ag_add_addons_options( $tabs ) {
			$tabs['options_pp'] = array(
				'label'    => __( 'Add-ons', 'yith-plugin-product-addons' ),
				'target'   => 'yith_wcpa_ag_data_tab',
				'class'    => array(),
				'priority' => 60,
			);
			return $tabs;
		}
		/**
		 * Print the addons panel in WC tabs
		 *
		 * @return void
		 */
		public function yith_wcpa_ag_print_addons_options() {
			global $post;
			$product = wc_get_product( $post->ID );
			$addons  = $product->get_meta( '_yith_wcpa_ag_addons' );
			echo '<div id="yith_wcpa_ag_data_tab" class="panel woocommerce_options_panel hidden">';
			yith_wcpa_get_view( '/addons-backend.php', compact( 'addons' ) );
			echo '</div>';
		}
		/**
		 * Save the addons
		 *
		 * @param  mixed $product
		 * @return void
		 */
		public function yith_wcpa_ag_save_addons_options() {
			global $post;
			$product = wc_get_product( $post->ID );
			if ( ! isset( $_POST['yith_wcpa_ag_addons'][0] ) ) {
				return;
			}
			foreach ( $_POST['yith_wcpa_ag_addons'][0] as $key => $addon ) {
				$addons[ $key ] = $addon;
			}
			if ( array_key_exists( '{{INDEX}}', $addons ) ) {
				unset( $addons['{{INDEX}}'] );
			}
			$this->sort_addons( $addons );

			array_walk( $addons, array( $this, 'sanitize_addons' ) );
			uasort( $addons, array( $this, 'sort_by_index' ) );
			$product->update_meta_data( '_yith_wcpa_ag_addons', $addons );
			$product->save();
		}
		/**
		 * Sort addons
		 *
		 * @param  mixed $addons
		 * @return void
		 */
		public function sort_addons( &$addons ) {
			foreach ( $addons as &$addon ) {
				$options = array();
				foreach ( $addon['options']['text'] as $key => $text ) {
					if ( ! ! $text ) {
						$options[] = array(
							'name'  => sanitize_text_field( wp_unslash( $text ) ),
							'price' => $addon['options']['price'][ $key ],
						);
					}
				}
				if ( ! $options ) {
					$options[] = array(
						'name'  => '',
						'price' => '',
					);
				}
				$addon['options'] = $options;
			}
		}
		/**
		 * Check if element1 is greater than element2
		 *
		 * @param  mixed $e1
		 * @param  mixed $e2
		 * @return void
		 */
		public function sort_by_index( $e1, $e2 ) {
			return intval( $e1['index'] ) > intval( $e2['index'] );
		}

		/**
		 * Sanitize the addon array
		 *
		 * @param  mixed $element
		 * @param  mixed $key
		 * @return void
		 */
		public function sanitize_addons( &$element, $key ) {
			if ( is_array( $element ) ) {
				array_walk( $element, array( $this, 'sanitize_addons' ) );
			} else {
				switch ( $key ) {
					case 'index':
					case 'free_char':
						$element = absint( $element );
						break;
					case 'price':
						$element = floatval( str_replace( ',', '.', $element ) );
						$element = intval( $element * 100 ) / 100;
						break;
					case 'enabled':
					case 'default_enabled':
						$element = 'yes' === $element ? 'yes' : 'no';
						break;
					default:
						$element = sanitize_text_field( wp_unslash( $element ) );
						break;
				}
			}
		}
		/**
		 * Add Add-ons in Product Variation.
		 *
		 * @param  mixed $loop
		 * @param  mixed $variation_data
		 * @param  mixed $variation
		 * @return void
		 */
		public function yith_wcpa_ag_add_addon_options_variations( $loop, $variation_data, $variation ) {
			$product_variation = wc_get_product( $variation );
			$addons            = $product_variation->get_meta( '_yith_wcpa_ag_addons' );
			$addons            = is_array( $addons ) ? $addons : array();
			yith_wcpa_get_view( '/addons-backend.php', compact( 'addons', 'loop' ) );
		}
		/**
		 * Save Add-ons from Variations
		 *
		 * @param  mixed $variation
		 * @param  mixed $i
		 * @return void
		 */
		public function yith_wcpa_ag_save_addon_options_variations( $variation, $i ) {
			$i++;
			if ( ! isset( $_POST['yith_wcpa_ag_addons'] ) || ! isset( $_POST['yith_wcpa_ag_addons'][ $i ] ) ) {
				return;
			}
			foreach ( $_POST['yith_wcpa_ag_addons'][ $i ] as $key => $addon ) {
				$addons[ $key ] = $addon;
			}
			if ( array_key_exists( '{{INDEX}}', $addons ) ) {
				unset( $addons['{{INDEX}}'] );
			}
			$this->sort_addons( $addons );
			array_walk( $addons, array( $this, 'sanitize_addons' ) );
			uasort( $addons, array( $this, 'sort_by_index' ) );
			$variation->update_meta_data( '_yith_wcpa_ag_addons', $addons );
		}
	}
}
