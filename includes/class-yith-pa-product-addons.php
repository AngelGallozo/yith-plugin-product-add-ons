<?php //phpcs:ignore
/**
 * This file belongs to the YITH PA Plugin Product Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PA_Plugin_Product_Addons' ) ) {
	/**
	 * YITH_PA_Plugin
	 */
	class YITH_PA_Plugin_Product_Addons {
		/**
		 * Main Instance
		 *
		 * @var YITH_PA_Product_Addons
		 * @since 1.0.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main Admin Instance
		 *
		 * @var YITH_PA_Plugin_Product_Addons_Admin
		 * @since 1.0.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_PA_Plugin_Product_Addons_Frontend
		 * @since 1.0.0
		 */
		public $frontend = null;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PA_Plugin_Product_Addons Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PA_Plugin_Product_Addons constructor.
		 */
		private function __construct() {
			$require = apply_filters( 'yith_pa_require_class',
				array(
					'common'   => array(
					'includes/functions.php',
					'includes/class-yith-pa-product-options.php',
					'includes/class-yith-pa-ajax.php',
					),
				'admin' => array(
								'includes/class-yith-pa-admin.php',
							),
				'frontend' => array(
								'includes/class-yith-pa-frontend.php',
							),
				)
			);
			$this->_require( $require );
			$this->init_classes();
			// Finally call the init function.
			$this->init();
		}
		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param  mixed $main_classes array The require classes file path
		 *
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 * @since  1.0.0
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_PA_DIR_PATH . $class ) ) {
						require_once( YITH_PA_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 * @since  1.0.0
		 **/
		public function init_classes() {
			YITH_PA_PO::get_instance();
			YITH_PA_AJAX::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 * @since  1.0.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_PA_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_PA_Frontend::get_instance();
			}
		}
	}
}
/**
 * Get the YITH_PA_Product_Addons instance
 *
 * @return YITH_PA_Plugin_Product_Addons
 */
if ( ! function_exists( 'yith_pa_plugin_product_addons' ) ) {
	function yith_pa_plugin_product_addons() {
		return YITH_PA_Plugin_Product_Addons::get_instance();
	}
}
