<?php //phpcs:ignore
/**
 * This file belongs to the YITH PA Plugin Product Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PA_Admin' ) ) {
	/**
	 * YITH_PA_Admin
	 */
	class YITH_PA_Admin {
		/**
		 * Main Instance
		 * @var YITH_PA_Admin
		 * @since 1.0.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Main plugin Instance
		 * @return YITH_PA_Admin Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PA_Admin constructor.
		 */
		private function __construct() {
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		}
		/**
		 * Enqueue_scripts
		 *
		 * @return void
		 */
		public function enqueue_scripts() {
			global $pagenow;
			if ( is_admin() && 'post.php' === $pagenow && isset( $_GET['post'] ) ) {
				wp_register_style( 'yith-pa-admin-css', YITH_PA_DIR_ASSETS_CSS_URL . '/yith-pa-admin-style.css', array(), YITH_PA_VERSION );
				wp_enqueue_script( 'yith-pa-admin-js', YITH_PA_DIR_ASSETS_JS_URL . '/yith-pa-admin-js.js', array( 'jquery' ), YITH_PA_VERSION, true );
				wp_enqueue_style( 'yith-pa-admin-css' );
			}
		}
	}
}
