<?php // phpcs:ignore
/**
 * This file belongs to the YITH PA Plugin Product Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PA_Frontend' ) ) {
	/**
	 * YITH_PA_Frontend
	 */
	class YITH_PA_Frontend {
		/**
		 * Main Instance
		 *
		 * @var YITH_PA_Frontend
		 * @since 1.0.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Main plugin Instance
		 * @return YITH_PA_Frontend Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PA_Frotend constructor.
		 */
		private function __construct() {
			// Add Addons in Product Page.
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_wcpa_ag_add_addons' ) );
			// Enqueue Scripts and Styles.
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_wcpa_ag_enqueue_scripts' ) );
			// Save Addons Data Cart.
			add_filter( 'woocommerce_add_cart_item', array( $this, 'yith_wcpa_ag_save_addons_data' ), 10, 2 );
			// Add Addons in Cart and CheckOut.
			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_wcpa_ag_add_addons_cart_page' ), 10, 2 );
			// Add Addons in Order.
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'yith_wcpa_ag_add_addons_order_page' ), 10, 4 );
			// Update price.
			add_filter( 'woocommerce_before_calculate_totals', array( $this, 'yith_wcpa_ag_update_product_price' ), 10, 4 );
		}
		/**
		 * Enqueue_scripts Function
		 *
		 * @return void
		 */
		public function yith_wcpa_ag_enqueue_scripts() {
			if ( is_product() ) {
				wp_register_style( 'yith-pa-frontend-css', YITH_PA_DIR_ASSETS_CSS_URL . '/yith-pa-frontend-style.css', array(), YITH_PA_VERSION );
				wp_register_script( 'yith-pa-frontend-js', YITH_PA_DIR_ASSETS_JS_URL . '/yith-pa-frontend-js.js', array( 'jquery' ), YITH_PA_VERSION, false );
				wp_enqueue_script( 'yith-pa-frontend-js' );
				wp_enqueue_style( 'yith-pa-frontend-css' );
				// Use Product Price in js.
				if ( function_exists( 'is_product' ) ) {
					$product = wc_get_product();
					wp_localize_script(
						'yith-pa-frontend-js',
						'yith_wcpa_ag',
						array(
							'product_price' => $product->get_price(),
							'ajaxurl'       => admin_url( 'admin-ajax.php' ),
						)
					);
				}
			}
		}
		/**
		 * Add Add-on in Product Page
		 *
		 * @return void
		 */
		public function yith_wcpa_ag_add_addons() {
			global $post;
			$product = wc_get_product( $post->ID );
			if ( ! $product ) {
				global $product;
			}
			if ( ! $product instanceof WC_Product ) {
				return;
			}
			$addons = $product->get_meta( '_yith_wcpa_ag_addons' );
			$addons = is_array( $addons ) ? $addons : array();
			wc_get_template( 'addons-frontend-show.php', compact( 'addons', 'product' ), '', YITH_PA_DIR_TEMPLATES_PATH . '/' );
		}		
		/**
		 * Add-ons Data in cart.
		 *
		 * @param  mixed $cart_item_data
		 * @param  mixed $cart_item_key
		 * @return void
		 */
		public function yith_wcpa_ag_save_addons_data( $cart_item_data, $cart_item_key ) {
			$product = $cart_item_data['data'];
			if ( $product->is_type( 'variation' ) ) {
				$addons          = $product->get_meta( '_yith_wcpa_ag_addons' );
				$addons          = ! ! $addons && is_array( $addons ) ? $addons : array();
				$general_product = wc_get_product( $product->get_parent_id() );
				$general_addons  = $general_product->get_meta( '_yith_wcpa_ag_addons' );
				$general_addons  = ! ! $general_addons && is_array( $general_addons ) ? $general_addons : array();
				$start_index     = count( $general_addons ) + 1;
				foreach ( $addons as &$addon ) {
					$addon['index'] += $start_index;
				}
				$addons = array_merge( $addons, $general_addons );
			} else {
				$addons = $product->get_meta( '_yith_wcpa_ag_addons' );
			}
			if ( isset( $_POST['yith_wcpa_ag_addons'] ) && isset( $addons ) ) {
				$addons_data = $_POST['yith_wcpa_ag_addons'];
				$addons_data = ! ! $addons_data && is_array( $addons_data ) ? $addons_data : array();
				foreach ( $addons as &$addon ) {
					$index = $addon['index'];
					if ( isset( $addon['enabled'] ) && 'yes' === $addon['enabled'] && isset( $addons_data[ $index ] ) ) {
						$name    = $addon['name'];
						$content = '';
						$price   = 0;
						switch ( $addon['field_type'] ) {
							case 'onoff':
							case 'checkbox':
								if ( 'yes' === $addons_data[ $index ] ) {
									$content = sanitize_text_field( $addon['name'] );
									if ( 'fixed' === $addon['price_settings'] ) {
										$price = floatval( $addon['price'] );
									}
								}
								break;
							case 'text':
							case 'textarea':
								$content = sanitize_text_field( wp_unslash( $addons_data[ $index ] ) );
								if ( 'free' !== $addon['price_settings'] ) {
									$price = floatval( $this->yith_wcpa_ag_calculate_price( $content, $addon ) );
								}
								break;
							case 'radio':
							case 'select':
								foreach ( $addon['options'] as $options => $option ) {
									if ( $option['name'] === $addons_data[ $index ] ) {
										$content = $option['name'];
										$price   = $option['price'];
									}
								}
								break;
						}
						if ( '' !== $content ) {
							$addons_save_cart[] = compact( 'name', 'content', 'price' );
						}
					}
				}
				$yith_wcpa_addons = array(
					'addons'     => $addons_save_cart,
					'base_price' => array(
						'content' => $product->get_price(),
						'name'    => __( 'Base price', 'yith-plugin-product-addons' ),
					),
				);
				$cart_item_data['yith_wcpa_ag_addons_cart'] = apply_filters( 'yith_wcpa_add_cart_item_data', $yith_wcpa_addons, $cart_item_data, $cart_item_key );
			}
			return $cart_item_data;
		}
		/**
		 * Calculate Price only text and textarea.
		 *
		 * @param  mixed $content
		 * @param  mixed $addon
		 * @return void
		 */
		public function yith_wcpa_ag_calculate_price( $content, $addon ) {
			$total_price = 0;
			$price_sett  = $addon['price_settings'];
			if ( ( 'free' !== $price_sett ) && ( 0 < strlen( $content ) ) ) {
				$price_data = $addon['price'];
				$text_price = floatval( $price_data );
				$free_chars = $addon['free_chars'];
				if ( strlen( $content ) > $free_chars ) {
					$total_price = 0;
					switch ( $price_sett ) {
						case 'fixed':
							$total_price = $text_price;
							break;
						case 'price-per-chars':
							$total_price = ( strlen( $content ) - $free_chars ) * $text_price;
							break;
					}
				}
			}
			return $total_price;
		}
		/**
		 * Add Add-on on Cart Page
		 *
		 * @param  mixed $item_data
		 * @param  mixed $cart_item_data
		 * @return void
		 */
		public function yith_wcpa_ag_add_addons_cart_page( $item_data, $cart_item_data ) {
			if ( isset( $cart_item_data['yith_wcpa_ag_addons_cart'] ) ) {
				$base_price  = $cart_item_data['yith_wcpa_ag_addons_cart']['base_price'];
				$item_data[] = array(
					'key'   => esc_html( $base_price['name'] ),
					'value' => sanitize_text_field( $base_price['content'] ),
				);
				foreach ( $cart_item_data['yith_wcpa_ag_addons_cart']['addons'] as $addon ) {
					if ( isset( $addon['name'] ) && isset( $addon['content'] ) && isset( $addon['price'] ) ) {
						$price       = $addon['price'];
						$item_data[] = array(
							'key'   => esc_html( $addon['name'] ) . ' (+ ' . wc_price( $price ) . ')',
							'value' => sanitize_text_field( $addon['content'] ),
						);
					}
				}
			}
			return $item_data;
		}
		/**
		 * Add Add-on on CheckOut and Order Page
		 *
		 * @param  mixed $item
		 * @param  mixed $cart_item_key
		 * @param  mixed $values
		 * @param  mixed $order
		 * @return void
		 */
		public function yith_wcpa_ag_add_addons_order_page( $item, $cart_item_key, $values, $order ) {
			if ( isset( $values['yith_wcpa_ag_addons_cart'] ) && ! empty( $values['yith_wcpa_ag_addons_cart'] ) ) {

				foreach ( $values['yith_wcpa_ag_addons_cart']['addons'] as $addons => $addon ) {

					$values = $addon['content'];

					$item->add_meta_data(
						$addon['name'] . ' (+ ' . wc_price( $addon['price'] ) . ')',
						$values,
						true
					);

				}
			}
		}
		/**
		 * Update Product Price.
		 *
		 * @param  mixed $cart
		 * @return void
		 */
		public function yith_wcpa_ag_update_product_price( $cart ) {
			$total_price  = 0;
			$price_addons = 0;
			foreach ( $cart->get_cart() as $cart_item_key => $item ) {

				if ( isset( $item['yith_wcpa_ag_addons_cart'] ) && ! empty( $item['yith_wcpa_ag_addons_cart'] ) ) {

					foreach ( $item['yith_wcpa_ag_addons_cart']['addons']  as $addon ) {
						$price_addons += floatval( $addon['price'] );
					}
					$price       = $item['data']->get_regular_price();
					$total_price = $price + $price_addons;
					if ( $price !== $total_price ) {
						$item['data']->set_price( $total_price );
					}
				}
			}
		}
	}
}
