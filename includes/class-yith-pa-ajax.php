<?php //phpcs:ignore
/**
 * This file belongs to the YITH PA Plugin Product Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PA_AJAX' ) ) {
	/**
	 * YITH_PP_AJAX
	 */
	class YITH_PA_AJAX {
		/**
		 * Main Instance
		 *
		 * @var YITH_PA_AJAX
		 * @since 1.0.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Main plugin Instance
		 * @return YITH_PA_AJAX Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PA_AJAX constructor.
		 */
		private function __construct() {
			add_action( 'wp_ajax_yith_wcpa_ag_get_variation_addons', array( $this, 'yith_wcpa_ag_get_variations_addons' ) );
			add_action( 'wp_ajax_nopriv_yith_wcpa_ag_get_variation_addons', array( $this, 'yith_wcpa_ag_get_variations_addons' ) );
		}
		/**
		 * Get Add-ons and Send Frontend in Product Page.
		 *
		 * @return void
		 */
		public function yith_wcpa_ag_get_variations_addons() {
			ob_start();
			if ( isset( $_POST['variation_id'] ) ) {
				$variation_id = intval( $_POST['variation_id'] );
				$product      = wc_get_product( $variation_id );
				$addons       = $product->get_meta( '_yith_wcpa_ag_addons' );

				foreach ( $addons as $addon ) {
					$addon['product_id'] = $variation_id;
					$addon['index']      += $_POST['start_id'];
					if ( isset( $addon['enabled'] ) && 'yes' === $addon['enabled'] ) {
						wc_get_template( $addon['field_type'] . '.php', compact( 'addon' ), '', YITH_PA_DIR_TEMPLATES_PATH . '/addon-fields/' );
					}
				}
			}
			wp_send_json( array( 'addons' => ob_get_clean() ) );
		}
	}
}
